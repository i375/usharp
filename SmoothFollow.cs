﻿using System;

namespace USharp
{
    class SmoothFollow : GameModule
    {
        private UnityEngine.Vector3 targetVector;
        private float targetScalar;

        private float currentScalar;
        private UnityEngine.Vector3 currentVector;

        private float smoothingRatio;
        private int targetFPS;

        public SmoothFollow()
        {
            targetVector = UnityEngine.Vector3.zero;
            targetScalar = 0F;

            currentVector = targetVector;
            currentScalar = targetScalar;
        }

        public void GameStep()
        {
            this.currentScalar = FollowScalarStep(
                currentScalar, 
                targetScalar, 
                targetFPS, 
                smoothingRatio, 
                UnityEngine.Time.deltaTime);

            this.currentVector = FollowVectorStep(
                currentVector,
                targetVector,
                targetFPS,
                smoothingRatio,
                UnityEngine.Time.deltaTime);
        }

        private static float FollowScalarStep(
            float currentScalar, 
            float targetScalar, 
            int targetFPS, 
            float smoothingRatio,
            float realTimeDelta)
        {

            if(targetScalar == 0)
            {
                var a = 1;
            }

            var timeDeltaForTargetFPS = 1F / targetFPS;

            var realTimeDeltaRatioToTargetFPSTimeDelta = realTimeDelta / timeDeltaForTargetFPS;

            var absCurrentScalarIsLessThanTargetScalar = Math.Abs(currentScalar) < Math.Abs(targetScalar);
            var absCurrentScalarIsMoreThanTargetScalar = !absCurrentScalarIsLessThanTargetScalar;

            var increment = (targetScalar - currentScalar) / smoothingRatio;

            var correctedIncrement = increment * realTimeDeltaRatioToTargetFPSTimeDelta;
            
            var smoothedScalar = currentScalar + correctedIncrement;

            var absSmoothedScalarIsMoreThanAbsTargetScalar = Math.Abs(smoothedScalar) > Math.Abs(targetScalar);
            var absSmoothedScalarIsLessThanAbsTargetScalar = !absSmoothedScalarIsMoreThanAbsTargetScalar;

            var smoothedScalarCheckedForOvershoot =
                absCurrentScalarIsLessThanTargetScalar && absSmoothedScalarIsMoreThanAbsTargetScalar
                ? targetScalar : smoothedScalar;

            var smoothedScalarCheckedForOvershootAndUndershoot =
                absCurrentScalarIsMoreThanTargetScalar && absSmoothedScalarIsLessThanAbsTargetScalar
                ? targetScalar : smoothedScalarCheckedForOvershoot;

            return smoothedScalarCheckedForOvershootAndUndershoot;
        }

        public void SetCurrent(float currentScalar)
        {
            this.currentScalar = currentScalar;
        }

        public void SetCurrent(UnityEngine.Vector3 currentVector)
        {
            this.currentVector = currentVector;
        }

        private static UnityEngine.Vector3 FollowVectorStep(
            UnityEngine.Vector3 currentVector,
            UnityEngine.Vector3 targetVector,
            int targetFPS,
            float smoothingRatio,
            float realTimeDelta)
        {
            var timeDeltaForTargetFPS = 1F / targetFPS;

            var realTimeDeltaRatioToTargetFPSTimeDelta = realTimeDelta / timeDeltaForTargetFPS;

            var currentVectorIsShorterThanTarget = currentVector.magnitude < targetVector.magnitude;
            var currentVectorIsLognerThanTarget = !currentVectorIsShorterThanTarget;

            var distanceBetweenTargetAndCurrent = targetVector - currentVector;

            var increment = distanceBetweenTargetAndCurrent / smoothingRatio;

            var correctedIncrement = increment * realTimeDeltaRatioToTargetFPSTimeDelta;
            
            var smoothedVector = currentVector + correctedIncrement;

            var smoothedVectorIsLongedThanTarget = smoothedVector.magnitude > targetVector.magnitude;
            var smoothedVectorIsShorterThanTarget = !smoothedVectorIsLongedThanTarget;

            var smoothedVectorCheckedForOverShoot =
                smoothedVectorIsLongedThanTarget && currentVectorIsShorterThanTarget
                ? targetVector : smoothedVector;
            
            var smootherVectorCheckedForOvershootAndUnderShoot =
                smoothedVectorIsShorterThanTarget && currentVectorIsLognerThanTarget
                ? targetVector : smoothedVectorCheckedForOverShoot;

            return smootherVectorCheckedForOvershootAndUnderShoot;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ratio">Higher value causes to follow slower, lower value causes to follow faster</param>
        public void SetSmoothingRatioAndTargetFPS(float ratio, int targetFPS)
        {
            if(ratio < 1)
            {
                throw new Exception("ratio must be >= 1");
            }
            
            smoothingRatio = ratio;

            this.targetFPS = targetFPS;
        }

        public void SetTarget(UnityEngine.Vector3 target)
        {
            targetVector = target;
        }

        public void SetTarget(float target)
        {
            targetScalar = target;
        }

        public float GetValueScalar()
        {
            return currentScalar;
        }

        public UnityEngine.Vector3 GetValueVector()
        {
            return currentVector;
        }
        
        #region Not needed - hidden
        public void GameStepFixed()
        {
            throw new NotImplementedException();
        }

        public void Render()
        {
            throw new NotImplementedException();
        }

        public void SetGameClass(GameClass gameCore)
        {
            // Skipping on purpose
        }
        #endregion
    }
}
