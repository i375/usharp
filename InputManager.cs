﻿using UnityEngine;

namespace USharp
{
    public class InputManager
    {

        public Vector3 GetMouseXYInGLView(Camera camera_Nullable)
        {
            Camera _cam = null;

            _cam = camera_Nullable ?? Camera.main;

            var pixelWidth = _cam.pixelWidth;
            var pixelHeight = _cam.pixelHeight;

            float mouseXNorm = Input.mousePosition.x / pixelWidth;
            float mouseYNorm = Input.mousePosition.y / pixelHeight;

            mouseXNorm = mouseXNorm * 2F - 1F;
            mouseYNorm = mouseYNorm * 2F - 1F;

            return new Vector3(mouseXNorm, mouseYNorm, 0.0F);
        }
        
        public Vector3 GetMouseXYWorldInOrthoCamera(Camera camera)
        {
            if (camera.orthographic == false)
            {
                Debug.LogWarning("Camera is not orthographic.");
                return Vector3.zero;
            }

            var mouseInGLView = GetMouseXYInGLView(camera);
            

            return camera.projectionMatrix.inverse.MultiplyPoint(mouseInGLView) + camera.transform.position;
        }

        public Vector3 GetMouseXYWorldInPerspectiveCamera(Camera camera)
        {
            if (camera.orthographic == true)
            {
                Debug.LogWarning("Camera is not perspetive.");
                return Vector3.zero;
            }

            var mouseInGLView = GetMouseXYInGLView(camera);

            return camera.projectionMatrix.inverse.MultiplyPoint(mouseInGLView) + camera.transform.position;
        }

    }
}