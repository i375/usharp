﻿using UnityEngine;

namespace USharp
{
    public class CounterTimer
    {
        private float counter = 0F;
        private bool isStopped = false;

        public float GetValue()
        {
            return counter;
        }

        public void Reset()
        {
            counter = 0F;
        }

        public void Stop()
        {
            isStopped = true;
        }

        public void Resume()
        {
            isStopped = false;
        }

        public void GameStep()
        {
            if(isStopped == false)
            {
                counter += Time.deltaTime;
            }
        }
    }
}
