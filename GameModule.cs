﻿using UnityEngine;
using System.Collections;


namespace USharp
{
    public interface GameModule
    {
        void SetGameClass(GameClass gameCore);
        void GameStep();
        void GameStepFixed();
        void Render();
    }
}

