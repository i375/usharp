﻿
namespace USharp.Geometry._2D
{
    class Star : Circle
    {
        public Star(float radius, int pointCount):base(radius/2F, pointCount*2)
        {
            CorrectVerticesToMakeCircleAStar();
            TransformVertices();
        }

        private void CorrectVerticesToMakeCircleAStar()
        {
            int starPointsCount = vertexBuffer.Count;
            const float starInnerPointScale = 2F;

            for(var starPointIndex = 1; starPointIndex < starPointsCount; starPointIndex += 2)
            {
                vertexBuffer[starPointIndex] = new UnityEngine.Vector3(
                    vertexBuffer[starPointIndex].x * starInnerPointScale,
                    vertexBuffer[starPointIndex].y * starInnerPointScale,
                    vertexBuffer[starPointIndex].z * starInnerPointScale);
            }
        }
    }
}
