﻿using UnityEngine;

namespace USharp.Geometry._2D
{
    class Line : Transform
    {
        public enum LineSide
        {
            Undefined,
            Left,
            Right
        }

        private VertexBuffer vertices;
        private Transform localTransform;

        public Line():base()
        {
            vertices = new VertexBuffer(2);
            localTransform = new Transform();
        }

        private const int FROM = 0;
        private const int TO = 1;

        public void SetVertices(Vector2 from, Vector2 to)
        {
            vertices[FROM] = from;
            vertices[TO] = to;
        }

        public LineSide OnWhichSideIsPointFromLine(Vector2 point)
        {
            var pointSide = LineSide.Left;

            var inverseTranformedPoint = TransformPointInverse(point);
            
            var lineVector = (vertices[TO] - vertices[FROM]).normalized;
            var pointVector = (inverseTranformedPoint - vertices[FROM]).normalized;

            var lineVectorAngleWithXAxis = Mathf.Atan2(lineVector.y, lineVector.x);

            localTransform.SetRotationRadiansAroundAngles(0F, 0F, -lineVectorAngleWithXAxis);

            lineVector = localTransform.TransformPoint(lineVector);
            pointVector = localTransform.TransformPoint(pointVector);

            if(pointVector.y > 0F)
            {
                pointSide = LineSide.Left;
            }

            if (pointVector.y < 0F)
            {
                pointSide = LineSide.Right;
            }

            if(pointVector.y == 0F)
            {
                pointSide = LineSide.Left;
            }

            return pointSide;
        }
    }
}
