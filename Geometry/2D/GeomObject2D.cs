﻿using UnityEngine;

namespace USharp.Geometry._2D
{
    public class GeomObject2D : Transform
    {
        public VertexBuffer vertexBuffer;
        public VertexBuffer vertexBufferTransformed;
        public int[] triangleIndices;

        private Triangulator triangulator;

        public GeomObject2D(int vertexCount) : base()
        {
            vertexBuffer = new VertexBuffer(vertexCount);
            vertexBufferTransformed = new VertexBuffer(vertexBuffer.Count);
        }

        public void TransformVertices()
        {
            for(var vertexIndex = 0; vertexIndex < vertexBuffer.Count; vertexIndex++)
            {
                vertexBufferTransformed[vertexIndex] = TransformPoint(vertexBuffer[vertexIndex]);
            }
        }

        public void TriangulateVertices()
        {
            triangulator = new Triangulator(vertexBuffer.GetAsVector2Array());
            triangleIndices = triangulator.Triangulate();
        }

        public override void SetPosition(float x, float y, float z)
        {
            base.SetPosition(x, y, z);    
            TransformVertices();
        }

        public override void SetPosition(Vector3 position)
        {
            base.SetPosition(position);
            TransformVertices();
        }

        public override void SetScale(float x, float y, float z)
        {
            base.SetScale(x, y, z);
            TransformVertices();
        }

        public override void SetRotationDegreesAroundAngles(float x, float y, float z)
        {
            base.SetRotationDegreesAroundAngles(x, y, z);
            TransformVertices();
        }

        public override void SetRotationRadiansAroundAngles(float x, float y, float z)
        {
            base.SetRotationRadiansAroundAngles(x, y, z);
            TransformVertices();
        }
        
    }
}
