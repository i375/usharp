﻿using UnityEngine;

namespace USharp.Geometry._2D
{
    class Triangle: GeomObject2D
    {
        private Line line;

        public Triangle(Vector2 p0, Vector2 p1, Vector2 p2):base(3)
        {
            line = new Line();
            
            vertexBuffer[0] = p0;
            vertexBuffer[1] = p1;
            vertexBuffer[2] = p2;

            TransformVertices();
            TriangulateVertices();
        }

        public Triangle(float radius):base(3)
        {
            line = new Line();

            var triangleVertex = new Vector3(0, radius);

            const int triangleVertexCount = 3;

            for(var triangleVertexIndex = 0; triangleVertexIndex < triangleVertexCount; triangleVertexIndex++)
            {
                var rotationZ = -(360 / triangleVertexCount * triangleVertexIndex);

                PushTransform();

                SetRotationDegreesAroundAngles(0F, 0F, rotationZ);

                vertexBuffer[triangleVertexIndex] = TransformPoint(triangleVertex);

                PopTransform();
            }

            TransformVertices();
            TriangulateVertices();
        }

        public bool ContainsPoint(Vector2 point)
        {
            var inverseTransformedPoint = TransformPointInverse(point);

            var containsPoint = true;

            Line.LineSide lineSide = Line.LineSide.Undefined;

            const int edgeCount = 3;

            for(var edgeIndex = 0; edgeIndex < edgeCount; edgeIndex++)
            {
                var fromVertexIndex = edgeIndex;
                var toVertexIndex = USharp.MathU.LoopIndex(edgeIndex + 1, edgeCount);

                line.SetVertices(vertexBuffer[fromVertexIndex], vertexBuffer[(int)toVertexIndex]);
                var newLineSide = line.OnWhichSideIsPointFromLine(inverseTransformedPoint);

                if(lineSide == Line.LineSide.Undefined)
                {
                    lineSide = newLineSide;
                }
                else if(lineSide != newLineSide)
                {
                    containsPoint = false;
                    break;
                }

            }

            return containsPoint;
        }

    }
}
