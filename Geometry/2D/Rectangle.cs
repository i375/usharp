﻿using UnityEngine;

namespace USharp.Geometry._2D
{
    public class Rectangle : GeomObject2D
    {
        private float width;
        public float Width
        {
            get
            {
                return width;
            }

            set
            {
                width = value;

                ComputeVertices();
            }
        }
        
        private float height;
        public float Height
        {
            get
            {
                return height;
            }

            set
            {
                height = value;

                ComputeVertices();
            }
        }
        
        
        public Rectangle(float width, float height):base(4)
        {

            this.width = width;
            this.height = height;

            ComputeVertices();

        }

        private const int TOP_LEFT = 0;
        private const int TOP_RIGHT = 1;
        private const int BOTTOM_RIGHT = 2;
        private const int BOTTOM_LEFT = 3;

        private void ComputeVertices()
        {
            var halfWidth = width / 2F;
            var halfHeight = height / 2F;

            vertexBuffer[TOP_LEFT] = new Vector3(-halfWidth, halfHeight);
            vertexBuffer[TOP_RIGHT] = new Vector3(halfWidth, halfHeight);
            vertexBuffer[BOTTOM_RIGHT] = new Vector3(halfWidth, -halfHeight);
            vertexBuffer[BOTTOM_LEFT] = new Vector3(-halfWidth, -halfHeight);

            TriangulateVertices();
            TransformVertices();

        }

        public bool ContainsPoint(Vector2 point)
        {
            var inverseTransformedPoint = TransformPointInverse(point);

            var pointIsOnTopAndOnRighSideOfBottomLeftVertex
                = inverseTransformedPoint.x > vertexBuffer[BOTTOM_LEFT].x
                && inverseTransformedPoint.y > vertexBuffer[BOTTOM_LEFT].y;

            var pointIsBelowAndOnLeftSideOfTopRightVertex
                = inverseTransformedPoint.x < vertexBuffer[TOP_RIGHT].x
                && inverseTransformedPoint.y < vertexBuffer[TOP_RIGHT].y;

            var pointIsInsideTheRectangle =
                pointIsOnTopAndOnRighSideOfBottomLeftVertex
                && pointIsBelowAndOnLeftSideOfTopRightVertex;



            return pointIsInsideTheRectangle;
        }

    }
}
