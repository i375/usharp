﻿
using UnityEngine;

namespace USharp.Geometry._2D
{
    class Circle : GeomObject2D
    {
        private float radius;

        public float Radius
        {
            get
            {
                return radius;
            }

            set
            {
                radius = value;
            }
        }

        public Circle(float radius, int vertexCount) : base(vertexCount)
        {
            if(!(vertexCount >= 8))
            {
                throw new System.Exception("vertexCount must be >= 8");
            }
            
            GenerateVertices(radius, vertexCount);
            TransformVertices();
            TriangulateVertices();

            this.radius = radius;
        }
        
        public void GenerateVertices(float radius, int vertexCount)
        {
            PushTransform();

            var angleStep = 360 / vertexCount;

            var baseVertex = new Vector2(radius, 0);

            for(var vertexIndex = 0; vertexIndex < vertexCount; vertexIndex++)
            {

                var currentAngle = - vertexIndex * angleStep;

                SetRotationDegreesAroundAngles(0F, 0F, currentAngle);

                vertexBuffer[vertexIndex] = TransformPoint(baseVertex);
            }

            PopTransform();
        }

        public bool ContainsPoint(Vector2 point)
        {
            var distance = Vector2.Distance(point, GetPosition());

            var circleContainsPoint = distance <= radius;

            return circleContainsPoint;
        }
    }
}
