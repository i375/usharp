﻿using System;
using System.IO;
using UnityEngine;

namespace USharp
{
    public class RandomsDatabase
    {
        private static byte[] randomBytes = null;
        private int uInt16counter = 0;

        public RandomsDatabase()
        {
            LoadData();
        }

        private void LoadData()
        {
            if(randomBytes == null)
            {
                var randomBytesTextAsset = Resources.Load("randomBytes") as TextAsset;
                randomBytes = randomBytesTextAsset.bytes;
            }
        }

        public void GetUInt16(ref int index, out UInt16 randomInt16)
        {
            var index_2 = MathU.LoopIndex(index * sizeof(UInt16), GetUInt16Count());

            randomInt16 = BitConverter.ToUInt16(randomBytes, (int)index_2);
        }

        public void GetNextUInt16(out UInt16 randomInt16)
        {
            GetUInt16(ref uInt16counter, out randomInt16);

            uInt16counter++;

            if(uInt16counter*2 > randomBytes.Length)
            {
                uInt16counter = 0;
            }
        }

        private UInt16 uint16_1 = 0;
        private float uint16Normalized_1 = 0F;

        public void GetNextUInt16(out UInt16 randomInt16, ref UInt16 minimum, ref UInt16 maximum)
        {
            GetNextUInt16(out randomInt16);

            uint16Normalized_1 = randomInt16 / (float)UInt16.MaxValue;

            uint16_1 = (UInt16)(maximum - minimum);

            randomInt16 = (UInt16)Mathf.RoundToInt(uint16Normalized_1 * uint16_1);

            randomInt16 = (UInt16)(randomInt16 + minimum);
        }

        public void ResetUInt16()
        {
            uInt16counter = 0;
        }

        public uint GetUInt16Count()
        {
            return (uint)(randomBytes.Length / sizeof(UInt16));
        }

        public void SetUInt16Index(int index)
        {
            uInt16counter = index;
        }
        

    }
}
