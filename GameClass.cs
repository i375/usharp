﻿using UnityEngine;
using System.Collections;

namespace  USharp
{

    public class GameClass : MonoBehaviour
    {
        private InputManager inputManager = new InputManager();

	    // Use this for initialization
	    void Start () {
	        OnStart();
	    }
	
	    // Update is called once per frame
	    void Update () {
            this.GameStep();
        }

        void FixedUpdate()
        {
            
            this.GameFixedUpdate();
        }

        public virtual void OnStart()
        {
            
        }

        public virtual void GameStep()
        {
            
        }

        public virtual void GameFixedUpdate()
        {
            
        }


    }

}
